#include <iostream>
#include <mupdf/fitz.h>


void rec_outline(fz_outline * outline, int level){

    for (fz_outline * o = outline; o != nullptr ;o = o->next)
    {
        for (int i = 0; i < level; ++i) {
            std::cout<<"\t";
        }

        std::cout<<o->title<<std::endl;

        if (o->down != nullptr){
            rec_outline(o->down,level+1);
        }

    }
}

int main(int argc, char *argv[]) {
    if (argc != 2){
        std::cout<< "Please put a pdf file name as argument."<<std::endl;
        exit(1);
    }

    char* filename = argv[1];

    fz_context *ctx;
    fz_document *doc;
    fz_outline * outline;

    ctx = fz_new_context(nullptr,nullptr, FZ_STORE_UNLIMITED);

    if(!ctx)
    {
        std::cerr << "Failed to create mupdf context";
    }

    fz_try(ctx) {
        fz_register_document_handlers(ctx);
        doc = fz_open_document(ctx, filename);
        outline= fz_load_outline(ctx,doc);
    }
    fz_catch(ctx) {
        std::cerr << fz_caught_message(ctx);
        exit(1);
    }

    rec_outline(outline,0);

    return 0;
}